import * as mariadb from "mariadb";

export function getMessages(room): Promise<any> {
  const pool = mariadb.createPool({
    host: process.env.DB_URL,
    port: parseInt(process.env.DB_PORT),
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: "react-chat",
    connectionLimit: 5,
  });
  let rows;
  const query = `SELECT * FROM messages WHERE room = '${room}' LIMIT 100`;
  return new Promise((resolve, reject) => {
    pool
      .getConnection()
      .then((conn) => {
        conn
          .query(query)
          .then((rows) => {
            return resolve(rows)
          })
          .catch((err) => {
            console.log(err);
            conn.end();
            return [];
          });
      })
      .catch((err) => {
        console.log(err);
        return [];
      });
  });
}
