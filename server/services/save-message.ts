import * as mariadb from 'mariadb';
import { ObjectId } from 'bson';

export function saveMessage(message, username, room, __createdtime__) {
  const pool = mariadb.createPool({
    host: process.env.DB_URL,
    port: parseInt(process.env.DB_PORT),
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: 'react-chat',
    connectionLimit: 5,
  });
  pool
    .getConnection()
    .then((conn) => {
      conn
        .query("SELECT 1 as val")
        .then((rows) => {
          return conn.query("INSERT INTO messages VALUES (?, ?, ?, ?, ?)", [new ObjectId(), message, username, room, new Date().toISOString().slice(0, 19).replace('T', ' ')]);
        })
        .then((res) => {
          conn.end();
        })
        .catch((err) => {
          console.log(err);
          conn.end();
        });
    })
    .catch((err) => {
      console.log(err)
    });
  return;
}
