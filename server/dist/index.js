"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
var express = require("express");
var app = express();
var http = require("http");
var cors = require("cors");
var server = http.createServer(app);
var Server = require("socket.io").Server;
var leaveRoomUtil = require("./utils/leave-room");
var get_messages_1 = require("./services/get-messages");
app.use(cors());
var io = new Server(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"],
    },
});
var CHAT_BOT = "ChatBot";
var chatRoom = "";
var allUsers = [];
io.on("connection", function (socket) {
    socket.on("join_room", function (data) {
        var username = data.username, room = data.room;
        socket.join(room);
        var __createdtime__ = Date.now();
        // Usuario entrou na sala
        socket.to(room).emit("receive_message", {
            message: "".concat(username, " has joined the chat room"),
            username: CHAT_BOT,
            __createdtime__: __createdtime__,
        });
        // Mensagem de bem vindo para o usuário
        socket.emit("receive_message", {
            message: "Bem vindo ".concat(username),
            username: CHAT_BOT,
            __createdtime__: __createdtime__,
        });
        // Salvando usuario na sala
        allUsers.push({ id: socket.id, username: username, room: room });
        var chatRoomUsers = allUsers.filter(function (user) { return user.room === room; });
        socket.to(room).emit("chatroom_users", chatRoomUsers);
        socket.emit("chatroom_users", chatRoomUsers);
        (0, get_messages_1.getMessages)(room).then(function (res) {
            res.map(function (row) { return row.__createdtime__ = new Date(row.__createdtime__); });
            socket.emit('last_100_messages', res);
        });
    });
    // Envio de mensagens
    socket.on("send_message", function (data) {
        var message = data.message, username = data.username, room = data.room, __createdtime__ = data.__createdtime__;
        io.in(room).emit("receive_message", data);
    });
    // Usuario deixou a sala
    socket.on("leave_room", function (data) {
        var username = data.username, room = data.room;
        socket.leave(room);
        var __createdtime__ = Date.now();
        allUsers = leaveRoomUtil(socket.id, allUsers);
        socket.to(room).emit("chatroom_users", allUsers);
        socket.to(room).emit("receive_message", {
            username: CHAT_BOT,
            message: "".concat(username, " has left the chat"),
            __createdtime__: __createdtime__,
        });
    });
    // Usuario desconectado
    socket.on("disconnect", function () {
        console.log("User disconnected from the chat");
        var user = allUsers.find(function (user) { return user.id == socket.id; });
        if (user === null || user === void 0 ? void 0 : user.username) {
            allUsers = leaveRoomUtil(socket.id, allUsers);
            socket.to(chatRoom).emit("chatroom_users", allUsers);
            socket.to(chatRoom).emit("receive_message", {
                message: "".concat(user.username, " has disconnected from the chat."),
            });
        }
    });
});
server.listen(4000, function () { return "Server is running on port 3000"; });
//# sourceMappingURL=index.js.map