function leaveRoom(userID, chatRoomUsers) {
    return chatRoomUsers.filter(function (user) { return user.id != userID; });
}
module.exports = leaveRoom;
//# sourceMappingURL=leave-room.js.map