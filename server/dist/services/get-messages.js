"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getMessages = void 0;
var mariadb = require("mariadb");
function getMessages(room) {
    var pool = mariadb.createPool({
        host: process.env.DB_URL,
        port: parseInt(process.env.DB_PORT),
        user: process.env.DB_USER,
        password: process.env.DB_PW,
        database: "react-chat",
        connectionLimit: 5,
    });
    var rows;
    var query = "SELECT * FROM messages WHERE room = '".concat(room, "' LIMIT 100");
    return new Promise(function (resolve, reject) {
        pool
            .getConnection()
            .then(function (conn) {
            conn
                .query(query)
                .then(function (rows) {
                return resolve(rows);
            })
                .catch(function (err) {
                console.log(err);
                conn.end();
                return [];
            });
        })
            .catch(function (err) {
            console.log(err);
            return [];
        });
    });
}
exports.getMessages = getMessages;
//# sourceMappingURL=get-messages.js.map