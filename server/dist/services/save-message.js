"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.saveMessage = void 0;
var mariadb = require("mariadb");
var bson_1 = require("bson");
function saveMessage(message, username, room, __createdtime__) {
    var pool = mariadb.createPool({
        host: process.env.DB_URL,
        port: parseInt(process.env.DB_PORT),
        user: process.env.DB_USER,
        password: process.env.DB_PW,
        database: 'react-chat',
        connectionLimit: 5,
    });
    pool
        .getConnection()
        .then(function (conn) {
        conn
            .query("SELECT 1 as val")
            .then(function (rows) {
            return conn.query("INSERT INTO messages VALUES (?, ?, ?, ?, ?)", [new bson_1.ObjectId(), message, username, room, new Date().toISOString().slice(0, 19).replace('T', ' ')]);
        })
            .then(function (res) {
            conn.end();
        })
            .catch(function (err) {
            console.log(err);
            conn.end();
        });
    })
        .catch(function (err) {
        console.log(err);
    });
    return;
}
exports.saveMessage = saveMessage;
//# sourceMappingURL=save-message.js.map