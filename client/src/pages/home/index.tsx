import styles from "./styles.module.css";
import { useNavigate } from "react-router-dom";

const Home = ({ username, setUsername, room, setRoom, socket }: any) => {
const navigate = useNavigate();

  const joinRoom = () => {
    if (room !== "" && username !== "") {
      socket.emit("join_room", { username, room });
      navigate("/chat", { replace: true });
    }
  };


  return (
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <h1>{`<>DevRooms</>`}</h1>
        <input
          className={styles.input}
          placeholder="Username"
          onChange={(e) => setUsername(e.target.value)}
        />

        <select
          className={styles.input}
          onChange={(e) => setRoom(e.target.value)}
        >
          <option>-- Selecione a sala --</option>
          <option value="piadas">Piadas</option>
          <option value="noticias">Notícias</option>
          <option value="livros">Livros</option>
          <option value="filmes">Filmes</option>
          <option value="esportes">Esportes</option>
          <option value="jogos">Jogos</option>
          <option value="ciencia">Ciência</option>
        </select>

        <button
          className="btn btn-darkgreen-theme"
          style={{ width: "100%" }}
          onClick={joinRoom}
          disabled={!room || !username}
        >
          Entrar na sala
        </button>
      </div>
    </div>
  );
};

export default Home;
