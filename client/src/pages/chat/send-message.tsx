import styles from "./styles.module.css";
import React, { useState } from "react";

const SendMessage = ({ socket, username, room }: any) => {
  const [message, setMessage] = useState("");

  const sendMessage = (event: React.FormEvent | React.MouseEvent) => {
    event.preventDefault()
    if (message !== "") {
      const __createdtime__ = Date.now();
      socket.emit("send_message", { id: socket.id, username, room, message, __createdtime__ });
      setMessage("");
    }
  };

  return (
    <form>
      <div className={styles.sendMessageContainer}>
        <input
          className={styles.messageInput}
          placeholder="Mensagem..."
          onChange={(e) => setMessage(e.target.value)}
          value={message}
        />
        <button className="btn btn-darkgreen-theme send-button" type="submit" onClick={e=> sendMessage(e)}>
          Enviar Mensagem
        </button>
      </div>
    </form>
  );
};

export default SendMessage;
