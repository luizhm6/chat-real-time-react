import styles from "./styles.module.css";
import MessagesReceived from "./messages";
import SendMessage from "./send-message";
import Sidebar from "./sidebar";

const Chat = ({ username, room, socket }: any) => {
  return (
    <div className={styles.chatContainer}>
      <Sidebar socket={socket} username={username} room={room} />
      <div>
        <MessagesReceived socket={socket} />
        <SendMessage socket={socket} username={username} room={room} />
      </div>
    </div>
  );
};

export default Chat;
