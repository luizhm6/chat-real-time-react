export interface messageType{
    id: string,
    message: string,
    username: string,
    __createdtime__: string
}