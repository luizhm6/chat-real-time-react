import { messageType } from "./models/message";
import styles from "./styles.module.css";
import { useState, useEffect, useRef } from "react";

const Messages = ({ socket }: any) => {
  const [messagesRecieved, setMessagesReceived] = useState<messageType[]>([]);
  const messagesColumnRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    socket.on("receive_message", (data: messageType) => {
      setMessagesReceived((state) => [
        ...state,
        {
          id: data.id,
          message: data.message,
          username: data.username,
          __createdtime__: data.__createdtime__,
        },
      ]);
    });

    return () => socket.off("receive_message");
  }, [socket]);

  // Ultimas mensagens
  useEffect(() => {
    socket.on("last_100_messages", (last100Messages: messageType[]) => {
      if(last100Messages?.length){
        last100Messages = sortMessagesByDate(last100Messages);
        setMessagesReceived((state) => [...last100Messages, ...state]);
      }
    });

    return () => socket.off("last_100_messages");
  }, [socket]);

  
  useEffect(() => {
    if(messagesColumnRef && messagesColumnRef.current){
      messagesColumnRef.current.scrollTop =
        messagesColumnRef.current.scrollHeight;
    }
  }, [messagesRecieved]);

  
  function sortMessagesByDate(messages: messageType[]) {
    return messages.sort(
      (a, b) => parseInt(a.__createdtime__) - parseInt(b.__createdtime__)
    );
  }
  function formatDateFromTimestamp(timestamp: any) {
    const date = new Date(timestamp);
    return date.toLocaleString();
  }

  return (
    <div className={styles.messagesColumn}  ref={messagesColumnRef}>
      {messagesRecieved.map((msg, i) => (
        <div
          className={
            styles.message +
            (msg.id === socket.id ? " self-message" : " group-message") +
            (msg.id ? "" : " server-message")
          }
          key={i}
        >
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <span className={styles.msgMeta}>{msg.username}</span>
            <span className={styles.msgMeta}>
              {formatDateFromTimestamp(msg.__createdtime__)}
            </span>
          </div>
          <p className={styles.msgText}>{msg.message}</p>
        </div>
      ))}
    </div>
  );
};

export default Messages;
