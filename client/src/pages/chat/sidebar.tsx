import styles from './styles.module.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { userType } from './models/users';

const Sidebar = ({ socket, username, room }: any) => {
  const [roomUsers, setRoomUsers] = useState<userType[]>();

  const navigate = useNavigate();

  useEffect(() => {
    socket.on('chatroom_users', (data: userType[]) => {
      setRoomUsers(data);
    });

    return () => socket.off('chatroom_users');
  }, [socket]);

  const leaveRoom = () => {
    const __createdtime__ = Date.now();
    socket.emit('leave_room', { username, room, __createdtime__ });
    navigate('/', { replace: true });
  };

  return (
    <div className={styles.roomAndUsersColumn}>
      <h2 className={styles.roomTitle}>{room}</h2>

      <div>
        {(roomUsers?.length || 0) > 0 && <h5 className={styles.usersTitle}>Users:</h5>}
        <ul className={styles.usersList}>
          {roomUsers?.map((user) => (
            <li
              style={{
                fontWeight: `${user.username === username ? 'bold' : 'normal'}`,
              }}
              key={user.id}
            >
              {user.username}
            </li>
          ))}
        </ul>
      </div>

      <button className='btn btn-darkgreen-theme' onClick={leaveRoom}>
        Deixar sala
      </button>
    </div>
  );
};

export default Sidebar;