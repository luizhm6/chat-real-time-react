# chat real time react

Nesta aplicação você poderá escolher uma sala, entrar nela e conversar de multiplas instâncias, se as portas estiverem abertas para devices externos poderá tambem conversar de dispositivos diferentes.

## Aplicação front end

Dentro da pasta "client" rodar os comandos "npm install" e "npm start"

## Servidor
Instalar globalmente o Typescript
Dentro da pasta "server" rodar os comandos "npm install", depois "tsc" e para rodar a aplicação rodar "npm run server"

## Database
```
CREATE DATABASE `react-chat` 
```
```
-- `react-chat`.messages definition

CREATE TABLE `messages` (
  `id` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `username` varchar(100) NOT NULL,
  `room` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
```